{ lib, inputs, system, ... }:

{
  # dell-xps = lib.nixosSystem {
  #   inherit system;
  #   specialArgs = { inherit inputs; };
  #   modules = [ ../system/machine/dell-xps ../system/configuration.nix ];
  # };

  # tongfang-amd = lib.nixosSystem {
  #   inherit system;
  #   specialArgs = { inherit inputs; };
  #   modules = [ ../system/machine/tongfang-amd ../system/configuration.nix ];
  # };

  dell-T7920 = lib.nixosSystem {
    inherit system;
    specialArgs = { inherit inputs; };
    modules = [ ../system/machine/dell-T7920 ../system/configuration.nix ];
  };
}
