{
  description = "Home Manager (dotfiles) and NixOS configurations";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixF.url = "github:nixos/nix?ref=master";
    nixos.url = "github:nixos/nixpkgs/master";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    hls.url = "github:haskell/haskell-language-server?ref=master";
    haskell-updates.url = "github:NixOS/nixpkgs/haskell-updates";
    smunix-pkgs.url = "github:smunix/nixpkgs-unfree?ref=main";
    colmena.url = "github:zhaofengli/colmena?ref=main";
    smunix-nur.url = "gitlab:smunix.nixos/nur-packages?ref=master";

    nurpkgs = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    doom-emacs = {
      url = "github:nix-community/nix-doom-emacs?ref=master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    tex2nix = {
      url = "github:Mic92/tex2nix/4b17bc0";
      inputs.utils.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, nixpkgs, nurpkgs, home-manager, tex2nix, doom-emacs
    , haskell-updates, smunix-pkgs, hls, colmena, smunix-nur, ... }:
    let system = "x86_64-linux";
    in {
      homeConfigurations = (import ./outputs/home-conf.nix {
        inherit system nixpkgs nurpkgs home-manager tex2nix;
      });

      nixosConfigurations = (import ./outputs/nixos-conf.nix {
        inherit (nixpkgs) lib;
        inherit inputs system;
      });

      devShell.${system} =
        (import ./outputs/installation.nix { inherit system nixpkgs; });
    };
}
