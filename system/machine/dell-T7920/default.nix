{ config, pkgs, ... }:

{
  imports = [
    # Hardware scan
    ./hardware-configuration.nix
    # v4l2
    ./v4l2.nix
  ];

  # Virtual Cam
  v4l2 = true;

  # Use the GRUB 2 boot loader.
  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      grub = {
        enable = false;
        device = "/dev/sdc"; # or "nodev" for efi only
        version = 2;
        useOSProber = true;
      };
    };
  };

  networking = {
    hostName = "dratrion";
    networkmanager.enable = true;
    useDHCP = false;
    interfaces = {
      enp0s31f6.useDHCP = true;
      enp2s0.useDHCP = true;
      wlp69s0.useDHCP = true;
    };
    hosts = { "49.12.70.26" = [ "rutwe" ]; };
  };

  services.xserver = {
    # xrandrHeads = [
    #   {
    #     output = "HDMI-1";
    #     primary = true;
    #     monitorConfig = ''
    #       Option "PreferredMode" "3840x2160"
    #       Option "Position" "0 0"
    #     '';
    #   }
    #   {
    #     output = "eDP-1";
    #     monitorConfig = ''
    #       Option "PreferredMode" "3840x2160"
    #       Option "Position" "0 0"
    #     '';
    #   }
    # ];
    # resolutions = [
    #   {
    #     x = 2048;
    #     y = 1152;
    #   }
    #   {
    #     x = 1920;
    #     y = 1080;
    #   }
    #   {
    #     x = 2560;
    #     y = 1440;
    #   }
    #   {
    #     x = 3072;
    #     y = 1728;
    #   }
    #   {
    #     x = 3840;
    #     y = 2160;
    #   }
    # ];
  };
}
